;;; Emacs Configuration
;; GUI Modes
(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)

;; Line numbers and hl line
(add-hook 'prog-mode-hook #'display-line-numbers-mode)
(global-hl-line-mode)

;; Splash screen
(setq inhibit-splash-screen t)

;; Files
(setq custom-file (concat user-emacs-directory "/site-lisp/custom.el"))
(unless (file-exists-p custom-file)
  (make-empty-file custom-file))
(load-file custom-file)

;; Font
(set-face-attribute 'default nil :font "CodeNewRoman Nerd Font" :height 100)

;; Packages setup
(load-file (concat user-emacs-directory "/site-lisp/setup.el"))

;; Basic Pakcages setup
(load-file (concat user-emacs-directory "/site-lisp/base.el"))

;; Programming setup
(load-file (concat user-emacs-directory "/site-lisp/lang.el"))

;; Programming setup
(load-file (concat user-emacs-directory "/site-lisp/general.el"))

