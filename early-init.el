;; Disable package.el in favor of straight.el
(setq package-enable-at-startup nil)

;; Removing backup files
(setq backup-directory-alist
      `(("." . ,(concat user-emacs-directory "backups"))))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

