;; Evil Mode setup
(use-package evil :defer t
  :init (evil-mode +1))

;; Smartparens
(use-package smartparens :defer t :diminish t
  :config (require 'smartparens-config)
  :init (smartparens-global-mode +1))

;; Vertical Completition
(use-package vertico :defer t
  :init (vertico-mode +1))

;; Theme
(use-package stimmung-themes :demand t :defer t
  :init (stimmung-themes-load-dark))

;; Changing theme based on time
(run-at-time "06:00" (* 60 60 24) (lambda () (stimmung-themes-load-light)))
(run-at-time "17:00" (* 60 60 24) (lambda () (stimmung-themes-load-dark)))

;; Dashboard
(use-package dashboard
  :config
  (setq dashboard-items '((recents  . 8)))
  ;; Emacsclient
  (setq initial-buffer-choice (lambda () (get-buffer-create "*dashboard*")))
  (setq dashboard-startup-banner "~/.config/emacs/resources/uefa.png")
  ;; Content is not centered by default. To center, set
  (setq dashboard-center-content t)
  
  ;; To disable shortcut "jump" indicators for each section, set
  (setq dashboard-show-shortcuts nil)
  :init (dashboard-setup-startup-hook))

;; Moodline
(use-package mood-line :init (mood-line-mode))
(setq mood-line-glyph-alist mood-line-glyphs-ascii)

