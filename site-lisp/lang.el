;; Treesitter
(use-package tree-sitter-langs :defer t
  :init (add-hook 'lsp-mode-hook #'tree-sitter-hl-mode))

;; LSP
(use-package lsp-ui :defer t)

;; C++
(use-package modern-cpp-font-lock :defer t
  :init (add-hook 'c++-mode-hook #'modern-c++-font-lock-mode))

;; Corfu
(use-package corfu
  :custom (corfu-auto t)                 ;; Enable auto completion
  :init (global-corfu-mode))

;; A few more useful configurations...
(use-package emacs
  :init
  ;; TAB cycle if there are only few candidates
  (setq completion-cycle-threshold 3)

  ;; Emacs 28: Hide commands in M-x which do not apply to the current mode.
  ;; Corfu commands are hidden, since they are not supposed to be used via M-x.
  ;; (setq read-extended-command-predicate
  ;;       #'command-completion-default-include-p)

  ;; Enable indentation+completion using the TAB key.
  ;; `completion-at-point' is often bound to M-TAB.
  (setq tab-always-indent 'complete))

;; Corfu icons
(use-package kind-icon
  :after corfu
  :custom (kind-icon-default-face 'corfu-default) ; to compute blended backgrounds correctly
  :config (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

;; Flycheck
(use-package flycheck :defer t
  :init (add-hook 'lsp-mode-hook #'flycheck-mode))

;; LSP Format
(eval-after-load 'lsp (add-hook 'before-save-hook #'lsp-format-buffer))

;; Font
(add-hook 'c++-mode-hook #'lsp)

;; Magit
(use-package magit :defer t)

;; Python
(use-package lsp-pyright
  :after lsp-mode
  :custom
  (lsp-pyright-auto-import-completions t)
  (lsp-pyright-typechecking-mode "on"))

(add-hook 'python-mode-hook #'lsp)
(setq company-dabbrev-downcase 0)
(setq company-idle-delay 0)

;; Typescript
(use-package typescript-mode :defer t)
(add-hook 'typescript-mode-hook #'lsp)
(add-to-list 'auto-mode-alist '("\\.jsx?\\'" . typescript-mode))

