;; General installation
(use-package general :defer t)

;; Fuzzy finder
(use-package fzf :defer t)
 (setq fzf/args "-x --color bw --print-query --margin=1,0 --no-hscroll"
        fzf/executable "fzf"
        fzf/git-grep-args "-i --line-number %s"
        ;; command used for `fzf-grep-*` functions
        ;; example usage for ripgrep:
        fzf/grep-command "rg --no-heading -nH"
        ;; fzf/grep-command "grep -nrH"
        ;; If nil, the fzf buffer will appear at the top of the window
        fzf/position-bottom t
        fzf/window-height 5)

;; Which-key
(use-package which-key :defer t :init (which-key-mode))

;; Keybinds
;; Preface: setting up function for sourcing emacs
(defun source-emacs ()
  "Source emacs config"
  (interactive)
  (load-file (concat user-emacs-directory "/init.el")))

(defun open-init ()
  "Open init.el"
  (interactive)
  (find-file (concat user-emacs-directory "/init.el")))

;; PART A: Set up 'SPC' as the global leader key
(general-create-definer ny
  :states '(normal insert visual emacs)
  :keymaps 'override
  :prefix "SPC" ;; set leader
  :global-prefix "M-SPC")

;; PART B: Configuration Sourcing and finding
(ny
  "," '(fzf :wk "Open fuzzy finder")
  "hrr" '(source-emacs :wk "Reload emacs config")
  "co" '(open-init :wk "Open Init.el")
  "." '(find-file :wk "Find File")
  "/" '(magit :wk "Open git"))

